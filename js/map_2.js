var map = [
    [-320, -320, 0.5],
    [-20, -200, 0.5],     //1
    [-20, -920, 0.5],    //2
    [-20, -1200, 0.5],    //3
    [-835, 0, 0.5],      //4
    [-320, -320, 1],   //5
    [-320, -320, 1],   //6
    [-1450, 0, 0.5],
    [-1450, -550, 0.5],
    [-1650, -1200, 0.5],
];
var i = 1, j = 1;
var flipFlop =[false,false,false,false] ;

var MapState = {
    
    create: function(){
            this.bg = game.add.sprite(-320, -320, 'background');
            this.bg.scale.setTo(0.30, 0.30);
            this.lock = game.add.sprite(150, 150, 'lock');
            this.lock.scale.setTo(0.5,0.5);
            this.control = {
                enter: game.input.keyboard.addKey(Phaser.Keyboard.ENTER),
            }
    },
    update: function(){
        this.moveMap(); 
    },
    moveMap: function() {
        this.cursor = game.input.keyboard.createCursorKeys();
        if(map[i*3+j+1][2]==1)
            this.lock.alpha = 0;
        else
            this.lock.alpha = 0.8;

        if (this.cursor.left.isDown){
            if (!flipFlop[0]) {
                if(i>0)
                    i--;
                flipFlop[0] = true;
            }
        }
    
        if (this.cursor.left.isUp) {
            flipFlop[0] = false;
        }

        if (this.cursor.right.isDown){
            if (!flipFlop[1]) {
                if(i<2)
                    i++;
                flipFlop[1] = true;
            }
        }
    
        if (this.cursor.right.isUp) {
            flipFlop[1] = false;
        }

        if (this.cursor.up.isDown){
            if (!flipFlop[2]) {
                if(j>0)
                    j--;
                flipFlop[2] = true;
            }
        }
    
        if (this.cursor.up.isUp) {
            flipFlop[2] = false;
        }

        if (this.cursor.down.isDown){
            if (!flipFlop[3]) {
                if(j<2){
                    if(i==1&&j==1){}
                    else    
                        j++;
                }
                flipFlop[3] = true;
            }
        }
    
        if (this.cursor.down.isUp) {
            flipFlop[3] = false;
        }

        switch(i) {
            case 0:
                if(j==0){
                    this.bg.scale.setTo(0.6, 0.6);
                    this.bg.x = map[1][0];
                    this.bg.y = map[1][1];
                    this.bg.alpha = map[1][2];
                }
                else if(j==1){
                    this.bg.scale.setTo(0.55, 0.55);
                    this.bg.x = map[2][0];
                    this.bg.y = map[2][1];
                    this.bg.alpha = map[2][2];
                }
                else{
                    this.bg.scale.setTo(0.55, 0.55);
                    this.bg.x = map[3][0];
                    this.bg.y = map[3][1];
                    this.bg.alpha = map[3][2];
                }
                break;

            case 1:
                if(j==0){
                    this.bg.scale.setTo(0.55, 0.55);
                    this.bg.x = map[4][0];
                    this.bg.y = map[4][1];
                    this.bg.alpha = map[4][2];
                }
                else if(j==1){
                    this.bg.scale.setTo(0.30, 0.30);
                    this.bg.x = map[5][0];
                    this.bg.y = map[5][1];
                    this.bg.alpha = map[5][2];
                    if(this.control.enter.isDown){
                        game.state.start("game");
                    }
                }
                else{
                    this.bg.scale.setTo(0.30, 0.30);
                    this.bg.x = map[6][0];
                    this.bg.y = map[6][1];
                    this.bg.alpha = map[6][2];
                    
                    j--;
                }
                break;

            default:
                if(j==0){
                    this.bg.scale.setTo(0.5, 0.5);
                    this.bg.x = map[7][0];
                    this.bg.y = map[7][1];
                    this.bg.alpha = map[7][2];
                }
                else if(j==1){
                    this.bg.scale.setTo(0.5, 0.5);
                    this.bg.x = map[8][0];
                    this.bg.y = map[8][1];
                    this.bg.alpha = map[8][2];
                }
                else{
                    this.bg.scale.setTo(0.55, 0.55);
                    this.bg.x = map[9][0];
                    this.bg.y = map[9][1];
                    this.bg.alpha = map[9][2];
                }

        }
        
    }

    
};
